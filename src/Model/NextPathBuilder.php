<?php

namespace LajiAuth\Model;


class NextPathBuilder
{
    /** @var string */
    private $nextPath;
    /** @var array */
    private $query = [];
    /** @var null|array */
    private $authenticationSource;
    /** @var string */
    private $language;
    /** @var boolean */
    private $allowUnapproved;
    /** @var NextPathExecutorInterface */
    private $executor;

    public function __construct($nextPath, $query, NextPathExecutorInterface $executor, $authenticationSource = null, $language = null, $allowUnapproved = false)
    {
        $this->nextPath = $nextPath;
        $this->query = $query;
        $this->executor = $executor;
        $this->allowUnapproved = (bool)$allowUnapproved;
        if ($authenticationSource !== null) {
            $this->authenticationSource = $authenticationSource;
        }
        if ($language !== null) {
            $this->language = $language;
        }
    }

    public function query($query) {
        return new NextPathBuilder($this->nextPath, $query, $this->executor, $this->authenticationSource, $this->language, $this->allowUnapproved);
    }

    public function authenticationSource($authenticationSource) {
        return new NextPathBuilder($this->nextPath, $this->query, $this->executor, $authenticationSource, $this->language, $this->allowUnapproved);
    }

    public function language($language) {
        return new NextPathBuilder($this->nextPath, $this->query, $this->executor, $this->authenticationSource, $language, $this->allowUnapproved);
    }

    public function allowUnapproved($allowUnapproved) {
        $allowUnapproved = (bool)$allowUnapproved;
        return new NextPathBuilder($this->nextPath, $this->query, $this->executor, $this->authenticationSource, $this->language, $allowUnapproved);
    }

    public function build() {
        return $this->executor->execute($this);
    }

    /**
     * @return string
     */
    public function getNextPath()
    {
        return $this->nextPath;
    }

    /**
     * @return array
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @return array|null
     */
    public function getAuthenticationSource()
    {
        return $this->authenticationSource;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @return boolean
     */
    public function isAllowUnapproved()
    {
        return $this->allowUnapproved;
    }
}