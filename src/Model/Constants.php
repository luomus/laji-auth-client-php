<?php

namespace LajiAuth\Model;


class Constants
{
    const TARGET_SYSTEM_PARAMETER = "target";
    const NEXT_PATH_PARAMETER = "next";
    const LANGUAGE_PARAMETER = "language";
    const AUTH_SOURCES = "auth-sources";
    const ALLOW_UNAPPROVED = "allowUnapproved";
    const SHIBBOLETH = 'shibboleth';
    const LOGIN_PATH = 'login';
    const TOKEN_PATH = 'token';
}