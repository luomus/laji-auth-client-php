<?php

namespace LajiAuth\Model;


interface NextPathExecutorInterface
{
    public function execute(NextPathBuilder $builder);
}