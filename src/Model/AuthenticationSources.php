<?php

namespace LajiAuth\Model;


class AuthenticationSources
{
    const HAKA = 'HAKA';
    const VIRTU = 'VIRTU';
    const FACEBOOK = 'FACEBOOK';
    const GOOGLE = 'GOOGLE';
    const LOCAL = 'LOCAL';

    public static $allSources = [
        self::HAKA,
        self::VIRTU,
        self::FACEBOOK,
        self::GOOGLE,
        self::LOCAL
    ];
}