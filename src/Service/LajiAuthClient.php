<?php

namespace LajiAuth\Service;


use LajiAuth\Model\AuthenticationSources;
use LajiAuth\Model\Constants;
use LajiAuth\Model\NextPathBuilder;
use LajiAuth\Model\NextPathExecutorInterface;
use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Uri\Http as HttpUri;

class LajiAuthClient implements NextPathExecutorInterface
{
    /** @var string */
    private $targetSystemId;

    /** @var string */
    private $lajiAuthUri;

    private $httpClient;

    private $httpClientOptions = [];

    public function __construct($targetSystemId, HttpUri $lajiAuthUri, $httpClientOptions = [])
    {
        if (empty($targetSystemId)) {
            throw new \InvalidArgumentException("system id must be provided");
        }
        $this->targetSystemId = $targetSystemId;
        $this->lajiAuthUri = $lajiAuthUri;
        $this->httpClientOptions = $httpClientOptions;
    }

    public function createLoginUrl($nextPath, array $query = [], $authenticationSource = null) {
        if ($authenticationSource !== null) {
            if (!in_array($authenticationSource, AuthenticationSources::$allSources)) {
                throw new \InvalidArgumentException("invalid login type '$authenticationSource' given");
            }
        }
        return new NextPathBuilder($nextPath, $query, $this, $authenticationSource);
    }

    private function build(NextPathBuilder $builder) {
        $query = [
            Constants::TARGET_SYSTEM_PARAMETER => $this->targetSystemId,
            Constants::NEXT_PATH_PARAMETER => $this->parseNext($builder->getNextPath(), $builder->getQuery()),
            Constants::ALLOW_UNAPPROVED => $builder->isAllowUnapproved() ? 'true' : 'false'
        ];
        if ($builder->getLanguage() !== null) {
            $query[Constants::LANGUAGE_PARAMETER] = $builder->getLanguage();
        }
        $path = '';
        if ($builder->getAuthenticationSource() !== null) {
            $path .= Constants::AUTH_SOURCES . '/';
            if ($this->isShibboleth($builder->getAuthenticationSource())) {
                $path .= Constants::SHIBBOLETH . '/';
            }
            $path .= $builder->getAuthenticationSource();
        } else {
            $path .= Constants::LOGIN_PATH;
        }
        $uri = new HttpUri($this->lajiAuthUri);
        $uri->setPath($uri->getPath() . $path);
        $uri->setQuery($query);
        return $uri;
    }

    private function isShibboleth($source) {
        return in_array($source, [AuthenticationSources::VIRTU, AuthenticationSources::HAKA ]);
    }

    private function parseNext($nextPath, array $nextQuery)
    {
        $uri = new HttpUri($nextPath);
        $query = $uri->getQuery() ?:[];
        $query = array_replace($query, $nextQuery);
        $uri->setQuery($query);

        return $uri->toString();
    }

    public function getTokenInfo($token)
    {
        if (!is_string($token)) {
            throw new \Exception('Token needs to be a string');
        }
        $uri = new HttpUri($this->lajiAuthUri);
        $uri->setPath($uri->getPath(). Constants::TOKEN_PATH . '/' . $token);
        $response = $this
            ->getHttpClient()
            ->setUri($uri)
            ->send();
        if (!$response->isSuccess()) {
            throw new ValidationException($response->getBody(), $response->getStatusCode());
        }
        try {
            $info = json_decode($response->getBody(), true);
        } catch (\Exception $e) {
            throw new ValidationException('Validation api did not return correct data');
        }
        if (!is_array($info)) {
            throw new ValidationException('Validation api did not return correct data');
        }
        if (isset($info['authenticationDetails']['targetSystemId']) &&
            $info['authenticationDetails']['targetSystemId'] !== $this->targetSystemId
        ) {
            throw new ValidationException('Authentication token target system id does not match the specified target system id');
        }
        return $info;
    }

    public function deleteToken($token)
    {
        if (!is_string($token)) {
            throw new \Exception('Token needs to be a string');
        }
        $uri = new HttpUri($this->lajiAuthUri);
        $uri->setPath($uri->getPath(). Constants::TOKEN_PATH . '/' . $token);
        $response = $this
            ->getHttpClient()
            ->setUri($uri)
            ->setMethod(Request::METHOD_DELETE)
            ->send();
        return $response->isSuccess();
    }

    public function execute(NextPathBuilder $builder)
    {
        return $this->build($builder);
    }

    /**
     * @return Client
     */
    private function getHttpClient()
    {
        if ($this->httpClient === null) {
            $this->httpClient = new Client(null, $this->httpClientOptions);
        }
        return $this->httpClient;
    }
}

class ValidationException extends \RuntimeException {}