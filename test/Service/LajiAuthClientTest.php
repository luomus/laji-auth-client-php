<?php

namespace LajiAuthTest\Service;


use LajiAuth\Model\AuthenticationSources;
use LajiAuth\Service\LajiAuthClient;
use Zend\Uri\Http;

class LajiAuthClientTest extends \PHPUnit_Framework_TestCase
{
    public function testGettingLoginUrl() {
        $client = new LajiAuthClient("KE.3", new Http('http://laji-auth.fi/'));
        /** @var \Zend\Uri\Uri $uri */
        $uri = $client->createLoginUrl('/seuraava/')->build();
        $this->assertInstanceOf('Zend\Uri\Uri', $uri);
        $this->assertEquals('http://laji-auth.fi:80/login?target=KE.3&next=%2Fseuraava%2F&allowUnapproved=false', $uri->toString());

        $uri = $client->createLoginUrl('/seuraava/', [], AuthenticationSources::VIRTU)->build();
        $this->assertInstanceOf('Zend\Uri\Uri', $uri);
        $this->assertEquals('http://laji-auth.fi:80/auth-sources/shibboleth/VIRTU?target=KE.3&next=%2Fseuraava%2F&allowUnapproved=false', $uri->toString());

        $uri = $client->createLoginUrl('/seuraava/', [], AuthenticationSources::LOCAL)->build();
        $this->assertInstanceOf('Zend\Uri\Uri', $uri);
        $this->assertEquals('http://laji-auth.fi:80/auth-sources/LOCAL?target=KE.3&next=%2Fseuraava%2F&allowUnapproved=false', $uri->toString());
    }
}